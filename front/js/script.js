(() => {
  const ENTER_CODE = 13;
  const ESC_CODE = 27;
  const ROW = 5;
  const URL_SERVER = 'http://api.t1.academy.dunice-testing.com/tasks';
  // const URL_SERVER = 'http://127.0.0.1:3000/tasks';

  let tasks = [];
  let currentPage = 1;
  let currentTab = 'all';

  const taskTitle = document.querySelector('.task-title');
  const addTaskButton = document.querySelector('.add-task-button');
  const tasksList  = document.getElementById('list-of-tasks');
  const paginationButtons = document.querySelector('.pagination');
  const generalCheckbox = document.querySelector('.select-all-tasks');
  const deleteAllSelectedButton = document.querySelector('.delete-selected-tasks');
  const countOfAll = document.querySelector('.count-of-all');
  const countOfActive = document.querySelector('.count-of-active');
  const countOfCompleted = document.querySelector('.count-of-complete');
  const tasksTabs = document.querySelector('.nav-tabs');
  const alert = document.querySelector('.alert');

  taskTitle.focus();

  const showError = (err) => {
    alert.textContent = err;
    alert.style.display = "block";
    setTimeout(() => {
      alert.style.display = 'none';
    }, 5000);
  };

  const receiveData = () => {
    fetch(URL_SERVER)
    .then(result => {
      if (result.ok) {
        return result.json();
      }
      throw new Error(result);
    })
    .then(data => {
      tasks = data;
      render();
    })
    .catch(error => showError(error));
  };

  const filteredByTab = () => {
    let filteredArray = [];
    switch (currentTab) {
      case 'active':
        filteredArray = tasks.filter((item) => !item.is_checked);
        return filteredArray;
      case 'completed':
        filteredArray = tasks.filter((item) => item.is_checked);
        return filteredArray;
      default:
        return tasks;
    }
  };

  const jumpIfEmpty = () => {
    const filteredArray = filteredByTab();
    const countPage = Math.ceil(filteredArray.length / ROW);
      if (!filteredArray.length) {
        currentTab = 'all';
        currentPage = 1;
      } else if (countPage < currentPage) {
        currentPage -= 1;
        currentTab = 'all';
      }
  };

  const selectAllTasks = () => {
    generalCheckbox.checked = tasks.length
      ? tasks.every((item) => item.is_checked)
      : false;
  };

  const countTasks = () => {
    countOfAll.textContent = tasks.length;
    countOfCompleted.textContent =  tasks.filter((item) =>  item.is_checked).length;
    countOfActive.textContent = countOfAll.textContent - countOfCompleted.textContent;
  };

  const render = () => {
    let taskLi = '';
    let paginationLi = '';
    const start = ROW * (currentPage - 1);
    const end = start + ROW;
    const filteredArray = filteredByTab();
    const itemsPerPage = filteredArray.slice(start, end);
    itemsPerPage.forEach((item) => {
      taskLi += `<li data-id=${item.id} class='text-center'>
      <input class='m-3 form-check-input' type='checkbox'
      ${item.is_checked ? 'checked' : ''}>
      <span class='m-2 title-task'>
      ${item.title}
      </span>
      <input class='hidden' type='text'>
      <button class='btn btn-danger btn-sm m-2 del-btn'>X</button>
    </li>`;
    });
    tasksList.innerHTML = taskLi;

    const countPage = Math.ceil(filteredArray.length / ROW);
    for (let i = 1; i <= countPage; i += 1) {
      paginationLi += i === currentPage 
      ? `<li class="page-item me-1 mt-1 show active pagination-element">
      <a class="page-link" href="#">${i}</a></li>`
      : `<li class="page-item me-1 mt-1 pagination-element">
      <a class="page-link" href="#">${i}</a></li>`;
    }
    paginationButtons.innerHTML = paginationLi;
    jumpIfEmpty();
    selectAllTasks();
    countTasks();
  };

  const paginate = (event) => {
    currentPage = Number(event.target.innerText);
    render();
  };

  const showTasks = (event) => {
    const { target: { classList } } = event;
    let tab = classList[1];
    let tabActive = classList[2];
    if (tab === 'tab-active-tasks') {
      currentTab = 'active';
      currentPage = 1;
    } 
    if (tab === 'tab-completed-tasks') {
      currentTab = 'completed';
      currentPage = 1;
    } 
    if (tab === 'tab-all-tasks') {
      currentTab = 'all';
      currentPage = 1;
    }
    tabActive = tabActive === 'active' ? '' : 'active';
    render();
  };

  const prepareString = (string) => {
    return _.escape(string.trim().replace(/\s+/g, " "));
  };

  const addTask = (value) => {
    fetch(URL_SERVER, {
      method: 'POST',
      headers: {
        'Content-type': 'application/json; charset=UTF-8',
        'accept': 'application/json'
      },
      body: JSON.stringify({ task: { title: value }})
    })
    .then(result => {
      if (result.ok) {
        return result.json();
      }
      throw new Error(result);
    })
    .then(data => {
      tasks.push(data);

      const countPage = Math.ceil(tasks.length / ROW);
      for (let i = 1; i <= countPage; i += 1) {
        currentPage = i + 1;
        render();
      }
      taskTitle.value = '';

      render();
    })
    .catch(error => showError(error));
  };

  const addTaskByButton = () => {
    const currentTitle = prepareString(taskTitle.value);
      if (currentTitle) {
        addTask(currentTitle);
      } 
  };

  const addTaskByEnterKey = (event) => {
    if (event.which === ENTER_CODE) {
      addTaskByButton();
    }
  };

  const deleteTask = (event) => {
    const { target: { parentNode: { dataset: {id} } } } = event;
    if (event.target.localName === 'button') {
      fetch(`${URL_SERVER}/${id}`, {
        method: 'DELETE',
        headers: {
          'Content-type': 'application/json; charset=UTF-8',
        }
      })
      .then(result => {
        if (result.ok) {
          return result;
        }
        throw new Error(result);
      })
      .then(() => {
          tasks = tasks.filter((item) => item.id !== Number(id));
          jumpIfEmpty();
          render();
        }
      )
      .catch(error => showError(error));
    }
  };

  const updateAll = (event, data) => {
    const { target: { parentNode: { children, dataset: { id } } } } = event;
    const inputEditTitle = children[2];
    const inputToggleHidden = inputEditTitle.classList.toggle("hidden");
    fetch(`${URL_SERVER}/${id}`, {
      method: 'PUT',
      headers: {
        'Content-type': 'application/json; charset=UTF-8',
        'accept': 'application/json'
      },
      body: JSON.stringify(data)
    })
    .then(result => {
      if (result.ok) {
        return result.json();
      }
      throw new Error(result);
    })
    .then((data) => {
      tasks.forEach((item) => {
        if (item.id === Number(id)) {
          item.is_checked = data.is_checked;
          const newTitle = prepareString(data.title);
          item.title = newTitle ? newTitle : item.title;
          inputToggleHidden;
          render();
        }
        
      });
    })
    .catch(error => showError(error));
  };
    

  const saveTitle = (event) => {
    const { target: { parentNode: { children } } } = event;
    const inputEditTitle = children[2].value;
    updateAll(event, {title: inputEditTitle});
  };

  const checkStatus = (event) => {
    const { target: { checked } } = event;
    if (event.target.type === 'checkbox') {
      updateAll(event, {is_checked: checked});
      jumpIfEmpty();
      render();
    }
  };

  const checkAllTasks = (event) => {
    const { target: { checked } } = event;
    fetch(URL_SERVER, {
      method: 'PUT',
      headers: {
        'Content-type': 'application/json; charset=UTF-8',
        'accept': 'application/json'
      },
      body: JSON.stringify({ is_checked: checked })
    })
    .then(result => {
      if (result.ok) {
        return result;
      }
      throw new Error(result);
    })
    .then(() => {
      tasks.forEach((item) => {
        item.is_checked = checked;
      });
      render();
    })
    .catch(error => showError(error));
  };

  const deleteSelected = () => {
    fetch(URL_SERVER, {
      method: 'DELETE',
    })
    .then(result => {
      if (result.ok) {
        return result;
      }
      throw new Error(result);
    })
    .then(() => {
      tasks = tasks.filter((item) => !item.is_checked);
      render();
      }
    )
    .catch(error => showError(error));
  };

  const editTitle = (event) => {
    const { target: { parentNode: { children, dataset: {id} } } } = event;
    const spanTitle = children[1];
    const inputEditTitle = children[2];
    const inputToggleHidden = inputEditTitle.classList.toggle('hidden');  
    tasks.forEach((item) => {
      if (item.id === Number(id)) {
        spanTitle.style.display = 'none';
        inputToggleHidden;
        inputEditTitle.value = spanTitle.textContent.trim();
        inputEditTitle.focus();
      }
    });
  };

  const cancelEditTitle = (event) => {
    const inputEditTitle = event.target.parentNode.children[2];
    const id = event.target.parentNode.dataset.id;
      tasks.forEach((item) => {
        if (item.id === Number(id)) {
          inputEditTitle.value = item.title;
          inputEditTitle.classList.toggle('hidden');
        }
      });
  };

  const callEventFunctions = (event) => {
    const { type, which, target: { localName, type: inputType, value } } = event;
    if (type === 'dblclick' && localName === 'span') {
      editTitle(event);
    } 
    if ((type === 'blur'
    || (type === 'keydown' && which === ENTER_CODE))
    && inputType === 'text' && value) {
      saveTitle(event);
    }
    if (type === 'keyup' && which === ESC_CODE) {
      cancelEditTitle(event);
    }
  };

  receiveData();

  addTaskButton.addEventListener('click', addTaskByButton);
  taskTitle.addEventListener('keydown', addTaskByEnterKey);
  generalCheckbox.addEventListener('click', checkAllTasks);
  deleteAllSelectedButton.addEventListener('click', deleteSelected);
  tasksList.addEventListener('click', deleteTask);
  tasksList.addEventListener('click', checkStatus);
  tasksList.addEventListener('dblclick', callEventFunctions);
  tasksList.addEventListener('blur', callEventFunctions, true);
  tasksList.addEventListener('keydown', callEventFunctions, false);
  tasksList.addEventListener('keyup', callEventFunctions);
  tasksTabs.addEventListener('click', showTasks);
  paginationButtons.addEventListener('click', paginate);
})();
