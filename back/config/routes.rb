Rails.application.routes.draw do
  resources :tasks
  put 'tasks', to: 'tasks#update_all'
  delete 'tasks', to: 'tasks#destroy_all_checked'
end
