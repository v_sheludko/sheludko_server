class TasksController < ApplicationController
  before_action :set_task, only: %i[ update destroy ]

  def index
    @tasks = Task.all

    render json: @tasks
  end

  def create
    @task = Task.new(task_params)

    if @task.save
      render json: @task, status: :created, location: @task
    else
      render json: @task.errors, status: :unprocessable_entity
    end
  end

  def update
    if @task.update(task_params)
      render json: @task
    else
      render json: @task.errors, status: :unprocessable_entity
    end
  end

  def update_all
    Task.update_all(is_checked: params.require(:is_checked))
  end

  def destroy
    @task.destroy
  end

  def destroy_all_checked
    Task.destroy_by(is_checked: true)
  end

  private
    def set_task
      @task = Task.find(params[:id])
    end

    def task_params
      params.require(:task).permit(:title, :is_checked)
    end
end
